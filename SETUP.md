# Prasyarat

Aplikasi berjalan dengan beberapa dependensi, yaitu:
- Node.js,
- Go,
- make,
- curl,
- jq,
- Git,
- Docker, dan
- Docker-Compose

Gunakan langkah-langkah berikut (Panduan dijalankan untuk Ubuntu Server 20.04).

## Instal Node.js
Versi yang direkomendasikan - 14.x
Node.js dapat diinstal secara langsung atau menggunakan nvm (pengelola versi node).
Ikuti langkah-langkah di bawah ini untuk menginstalnya menggunakan nvm.

### Instal nvm
Catatan:
Periksa apakah file bashrc ada, menggunakan `ls ~/.bashrc`. Jika tidak ada, tambahkan file menggunakan `touch ~/.bashrc`.
Periksa apakah file profil (pengguna saat ini) ada, menggunakan `ls ~/.profile`. Jika tidak ada, tambahkan file menggunakan `touch ~/.profile`.

Untuk ubuntu:
```
sudo apt-get install build-essential libssl-dev -y
curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.0/install.sh | bash
source ~/.bashrc
source ~/.profile
eksport NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"
```
### Instal versi Node.js yang diperlukan
```
nvm install 14
nvm use 14
```

## Instal Golang
Kontrak pintar (smart contract) yang digunakan ditulis menggunakan Go. Ikuti langkah-langkah di bawah ini untuk menginstalnya (Versi 1.16 direkomendasikan)

Untuk ubuntu:
```
wget https://dl.google.com/go/go1.16.5.linux-amd64.tar.gz
sudo rm -rf /usr/local/go && sudo tar -C /usr/local -xzf go1.16.5.linux-amd64.tar.gz
```
Periksa apakah file .profile ada (untuk instalasi pengguna saat ini) menggunakan `ls ~/.profile`. Jika tidak ada, tambahkan file menggunakan `touch ~/.profile`. Buka file menggunakan editor vi `vi ~/.profile`. Tambahkan baris berikut di akhir file.
`eksport PATH=$PATH:/usr/local/go/bin`. Kirimkan perintah Sumber profil `source ~/.profile`. Periksa apakah go sudah berhasil diinstal `go version`

**Catatan masalah**: Jika Anda memiliki kesalahan "izin ditolak/permission denied" dalam menghapus/untarring tar file, pastikan untuk memiliki kepemilikan direktori yang benar, atau Anda mungkin ingin menggunakan `sudo` untuk untar.
Untuk detail lebih lanjut atau pemecahan masalah, lihat https://golang.org/doc/install

## Instal Git
Untuk ubuntu:
```
sudo apt install git-all
```

## Instal Docker
Untuk ubuntu:
```
sudo apt-get install apt-transport-https ca-sertifikat curl gnupg-agent software-properties-common -y
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key tambahkan -
sudo apt-key fingerprint 0EBFCD88
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
sudo apt-get install docker-ce docker-ce-cli containerd.io -y
```
### Tambahkan akun pengguna ke grup docker
```
sudo groupadd docker
sudo usermod -aG docker $(whoami)
grup $USER
sudo apt-get update -y
```
Silakan ikuti dokumentasi yang diberikan di bawah ini untuk memastikan Anda memiliki Docker dan Anda telah mengaturnya dengan cara yang benar:
https://hyperledger-fabric.readthedocs.io/en/release-2.2/prereqs.html#docker-and-docker-compose

Untuk detail lebih lanjut dan pemecahan masalah, silakan merujuk ke
https://hyperledger-fabric.readthedocs.io/en/release-2.2/prereqs.html#

## Instal Docker Compose
Untuk Ubuntu:
```
sudo apt-get install docker-compose -y
```
**Catatan pemecahan masalah**: Jika Anda memiliki masalah izin untuk menjalankan docker, jalankan `newgrp docker` dan coba lagi.

## Instal Make
Untuk Ubuntu:
```
sudo apt-get install build-essential
```

## Instal jq
Untuk Ubuntu:
```
sudo apt-get install jq -y
```

## Instal Curl
Untuk Ubuntu:
```
sudo apt-get install curl -y
```

# Memulai aplikasi
Setelah prasyarat diinstal, ikuti petunjuk di bawah ini untuk memulai aplikasi pada mesin lokal.

1. Unduh atau Klon repo kode.
    git clone https://gitlab.com/abbeet/galeri.git

2. Buka terminal dari direktori root repo kode dan jalankan jaringan blockchain menggunakan perintah berikut:
    ```
    cd galeri
    cd auction-restapi/network/local
    ./start.sh
    ```
    Script akan membuat jaringan Fabric sederhana. Jaringan memiliki dua organisasi peer dengan masing-masing dua peer dan node tunggal layanan pemesanan.

3. Setelah pengaturan jaringan blockchain selesai, mulai server backend Node.js menggunakan perintah berikut:
    ```
    cd auction-restapi/node
    npm install
    npm start
    ```
    Aplikasi node sekarang harus aktif dan berjalan di `localhost:3001`.

4. Buka terminal lain dari direktori root repo kode dan mulai aplikasi UI menggunakan perintah berikut:
    ```
    cd auction-ui
    npm install
    npm start
    ```
    Aplikasi front-end sekarang harus aktif dan berjalan. Aplikasi ini sekarang dapat diakses dari browser web @`localhost:3000`.

5. Buka terminal lain dari direktori root repo kode dan mulai aplikasi UI menggunakan perintah berikut:
    ```
    cd sis
    pip install -r requirements.txt
    python server.py
    ```
    Aplikasi plagiat sekarang harus aktif dan berjalan. Aplikasi ini sekarang dapat diakses dari browser web @`localhost:5555`.
