# MAD-K (Modul Aset Digital Kebudayaan)
Galeri Dangau -- Modul Aset Digital Kebudayaan adalah Aplikasi yang dikembangkan oleh Kelompok 8 dalam Tugas Besar Mata Kuliah Perancangan Layanan Teknologi Informasi, Magister Teknik Elektro - ITB 2022. Aplikasi ini berbasiskan blockchain dalam transaksinya, serta membuat NFT untuk setiap Aset Digital yang dikelola. Selain itu untuk setiap Aset Digital yang diupload akan di cek plagiarismenya.

Aplikasi akan berjalan dengan modul dan port sebagai berikut:

| No  | Aplikasi            | Alamat              |   
| --- | ------------------- | ------------------  |
| 1   | Aplikasi Node       | localhost:3001      |
| 2   | Aplikasi Front-End  | localhost:3000      |
| 3   | Aplikasi Plagiat    | localhost:5555      |
| 4   | Aplikasi Swagger    | localhost:3000/dist |

Contoh aplikasi yang berjalan di domain publik adalah http://dangau.id

# Source
Galeri Dangau | MAD-K dikembangkan dengan menggunakan NFT-Auction (Hyperledger Fabric) sebagai node blockchain, React App untuk Front-End, Swagger Dist untuk API explorer, dan Simple Image Search untuk Deteksi Plagiat.

- https://github.com/hyperledger-labs/nft-auction
- https://github.com/facebookincubator/create-react-app
- https://github.com/matsui528/sis
- https://github.com/swagger-api/swagger-ui/tree/master/dist

# Instalasi
Silahkan baca [SETUP](SETUP.md)
