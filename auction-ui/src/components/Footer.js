import React, { Component } from 'react';


class Footer extends Component {

    // constructor(props) {
    //     super(props);
    // }

    render() {
        return (
            <div className="d-flex my-1 justify-content-center">
                <span>&copy; {new Date().getFullYear()} Powered by <a href="https://dangau.id:3000/" target="_blank" rel="noopener noreferrer">Kelompok 8 PLTI</a></span>
            </div>
        );
    }

}

export default Footer;
